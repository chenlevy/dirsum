from ubuntu

run apt update && apt install -y python3 python3-pip
run mkdir -p /app/dirsum
copy requirements.txt /app
run python3 -m pip install -r /app/requirements.txt
copy dirsum /app/dirsum
env LC_ALL C.UTF-8
env LANG C.UTF-8
expose 8000
entrypoint cd /app && uvicorn --host=0.0.0.0 dirsum.api:app
