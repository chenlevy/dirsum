from typing import List

import pytest

from dirsum import my_python_script as mut  # module under test


@pytest.fixture
def sample_dir(tmpdir):
    """create a sample tree under root such as

    $tmpdir/ (a pytest fixture)
    ├── baz/
    ├── foo/
    │   ├── a
    │   └── bar/
    │       ├── a
    │       └── b
    └── qux/
        └── quux/
            └── c

    """
    # create directories
    baz = tmpdir / "baz"
    foo = tmpdir / "foo"
    foo_bar = foo / "bar"
    qux = tmpdir / "qux"
    qux_quux = qux / "quux"

    baz.mkdir()
    foo.mkdir()
    foo_bar.mkdir()
    qux.mkdir()
    qux_quux.mkdir()

    (foo / "a").write("file a")
    (foo_bar / "a").write("file a")
    (foo_bar / "b").write("file b")
    (qux_quux / "c").write("file c")

    yield tmpdir


def assert_same_lists(actual: List[str], prefix: str, expected: List[str]):
    # verify that all the actual items starts with the prefix
    for item in actual:
        assert item.startswith(prefix)

    # assert that all the actual items w/o the prefix are the expected set
    prefix_len = len(prefix)
    actual_set_wo_prefix = set(item[prefix_len:] for item in actual)
    assert actual_set_wo_prefix == set(expected)


def test_files_in_dir(sample_dir):
    prefix = str(sample_dir)
    actual = list(mut.files_in_dir(prefix))
    expected = ["/foo/a", "/foo/bar/a", "/foo/bar/b", "/qux/quux/c"]
    assert_same_lists(actual, prefix, expected)


def test_hash_file(sample_dir):
    file_a = str(sample_dir / "foo" / "a")
    assert mut.hash_file(file_a) == "ae407aff5e6bbe56ba0373399eaf6a9f"
    other_file_a = str(sample_dir / "foo" / "bar" / "a")
    assert mut.hash_file(other_file_a) == mut.hash_file(file_a)
    file_b = str(sample_dir / "foo" / "bar" / "b")
    assert mut.hash_file(file_b) == "0de2df12d07700d0e6582e84f27a7d5e"
    file_c = str(sample_dir / "qux" / "quux" / "c")
    assert mut.hash_file(file_c) == "b5316ce018d8eaf1212c3d087df1f5c3"


def test_hased_file(sample_dir):
    file_a = str(sample_dir / "foo" / "a")
    ans = mut.hashed_file(file_a)
    assert ans.path == file_a
    assert ans.hash == "ae407aff5e6bbe56ba0373399eaf6a9f"


def test_checksum_dir(sample_dir):
    prefix = str(sample_dir)
    actual = [f"{f.path},{f.hash}" for f in mut.checksum_dir(sample_dir)]
    expected = [
        "/foo/a,ae407aff5e6bbe56ba0373399eaf6a9f",
        "/foo/bar/a,ae407aff5e6bbe56ba0373399eaf6a9f",
        "/foo/bar/b,0de2df12d07700d0e6582e84f27a7d5e",
        "/qux/quux/c,b5316ce018d8eaf1212c3d087df1f5c3",
    ]
    assert_same_lists(actual, prefix, expected)


# def test_enumerate_checksum_dir_single_file(sample_dir):
#     ans = mut.enumerated_checksum_dir(str(sample_dir / "foo" / "bar" / "b"))
#     assert ans[0] == 1
#     assert ans[1].startswith(str(sample_dir))
#     assert ans[1][len(str(sample_dir))] == "/bar/baz/b"
#     assert ans[2] == "0de2df12d07700d0e6582e84f27a7d5e"
