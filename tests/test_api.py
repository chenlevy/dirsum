from fastapi.testclient import TestClient
import pytest

from dirsum import api as mut
from .test_my_python_script import sample_dir, assert_same_lists


@pytest.fixture
def client():
    return TestClient(mut.app)


def test_read_directory_checksums(sample_dir, client):
    resp = client.get("/directory", params=dict(path=str(sample_dir)))
    assert resp.status_code == 200
    ans = resp.json()

    # we test each part of the inner list, to deal with unpredictable path prefix.
    numbers = [n for (n, _, _) in ans]
    assert numbers == [1, 2, 3, 4]

    paths = [f for (_, f, _) in ans]
    assert_same_lists(
        paths,
        str(sample_dir),
        ["/foo/a", "/foo/bar/a", "/foo/bar/b", "/qux/quux/c"],
    )

    hashes = [h for (_, _, h) in ans]
    assert set(hashes) == set(
        [
            "ae407aff5e6bbe56ba0373399eaf6a9f",
            "0de2df12d07700d0e6582e84f27a7d5e",
            "b5316ce018d8eaf1212c3d087df1f5c3",
        ]
    )


def test_read_file_checksum(sample_dir, client):
    resp = client.get("/file", params=dict(path=str(sample_dir / "foo" / "a")))
    assert resp.status_code == 200
    assert resp.json() == "ae407aff5e6bbe56ba0373399eaf6a9f"


def test_verify_file_checsum_ok(sample_dir, client):
    resp = client.get(
        "/verify",
        params=dict(
            path=str(sample_dir / "foo" / "a"),
            checksum="ae407aff5e6bbe56ba0373399eaf6a9f",
        ),
    )
    assert resp.status_code == 200
    assert resp.json() == "OK"


def test_verify_file_checsum_bad_hash(sample_dir, client):
    resp = client.get(
        "/verify",
        params=dict(
            path=str(sample_dir / "foo" / "a"),
            checksum="ae407aff5e6bbe56ba0373399eaf6a9e",  # last char should be `f`
        ),
    )
    assert resp.status_code == 400
    assert resp.json() == {"detail": "FAIL"}
