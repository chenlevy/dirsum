======
dirsum
======

.. image:: https://gitlab.com/chenlevy/dirsum/badges/master/pipeline.svg
   :target: https://gitlab.com/chenlevy/dirsum/-/commits/master
   :alt: pipeline status

.. image:: https://gitlab.com/chenlevy/dirsum/badges/master/coverage.svg
   :target: https://gitlab.com/chenlevy/dirsum/-/commits/master
   :alt: coverage report

Generate a checksum list for files under a given directory (and its sub-directories).

Usage
=====

Run the script
--------------

To run the script locally use::

  ./dirsum/my_python_script.py <path>

Run the server
--------------

You can run the server as a process with::

  uvicorn dirsum.api:app --reload

Or run the service inside a docker container with::

  make docker-run

API Documentation
-----------------

See http://localhost:8000/docs/

Installation
============

Within a virtual environment
----------------------------

The script use only the Python's standard libraries, so it need no installation, after cloning this repository, whoever for the service version you will need to install the dependencies (see blow).  The recommended way for running the service process is to use a virtual environment:

1. Create a virtualenv with::

     python3 -m virtualenv .venv

2. Activate the venv::

     source .venv/bin/activate

3. Install dependencies::

     pip3 install -r requirements.txt

   For running the unit-test you will also need to run::

     pip3 install -r dev_requirements.txt


Requirements
------------

- This script requires `Python <https://python.org>`_ >= 3.6.
- The server also requires `FastAPI <https://fastapi.tiangolo.com>`_ and `Uvicorn <https://www.uvicorn.org/>`_ (see ``requirements.txt``)
- For running the unit test you will also need `PyTest <https://docs.pytest.org/en/latest/>`_.

Licence
=======

`Python Software Foundation License <https://docs.python.org/3/license.html#terms-and-conditions-for-accessing-or-otherwise-using-python>`_

Authors
=======

`dirsum` was written by `Chen Rotem Levy <chen@rotemlevy.name>`_.
