import setuptools

setuptools.setup(
    name="dirsum",
    version="1.0.0",
    url="https://gitlab.com/chenlevy/dirsum",

    author="Chen Rotem Levy",
    author_email="chen@rotemlevy.name",

    description="Generate a checksum list for filesectory",
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(),

    install_requires=["fastapi", "uvicorn"],
    extras_require={
        'dev': [
            'pytest',
            'pytest-cov',
            'black',
        ]
    },

    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)
