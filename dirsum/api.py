from os.path import isdir, isfile
from fastapi import FastAPI, HTTPException
from dirsum import my_python_script as lib, __version__

app = FastAPI(
    title="DirSum",
    description="Directory recursive checksum generator as a micro-service",
    version=__version__,
)


@app.get("/directory")
def read_directory_checksums(path: str):
    if isdir(path):
        return lib.enumerated_checksum_dir(path)
    else:
        raise HTTPException(status_code=404)


@app.get("/file")
def read_file_checksum(path: str):
    if isfile(path):
        return lib.hash_file(path)
    else:
        raise HTTPException(status_code=404)


@app.get("/verify")
def read_verify_file_checksum(checksum: str, path: str):
    if not (isfile(path) and lib.hash_file(path) == checksum):
        raise HTTPException(status_code=400, detail="FAIL")
    return "OK"
