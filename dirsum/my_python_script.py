#!/usr/bin/env python3
from collections import namedtuple
from hashlib import md5 as hasher
from multiprocessing import Pool
from os import scandir, cpu_count
from typing import Iterator, Tuple

CHUNK_SIZE = 4096  # this should be the block size of the underling filesystem


def files_in_dir(path: str) -> Iterator[str]:
    """Return a list of path-names to all the files in a given directory
    """

    for f in scandir(path):
        if f.is_file():
            yield f.path
        elif f.is_dir():
            yield from files_in_dir(f.path)


def hash_file(file_path: str) -> str:
    """return a hex representation of the hash hashed file"""
    h = hasher()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(CHUNK_SIZE), b""):
            h.update(chunk)
    return h.hexdigest()


HashedFile = namedtuple("HashedFile", "path hash")


def hashed_file(file_path) -> HashedFile:
    return HashedFile(file_path, hash_file(file_path))


def checksum_dir(path: str) -> Iterator[HashedFile]:
    """use a thread pool to get checksums of files in path"""
    with Pool(cpu_count()) as p:
        return p.map(hashed_file, files_in_dir(path))


def enumerated_checksum_dir(path: str) -> Iterator[Tuple[int, str, str]]:
    return (
        (n, f.path, f.hash)
        for (n, f) in enumerate(checksum_dir(path), start=1)
    )


if __name__ == "__main__":
    from sys import argv

    for (n, p, h) in enumerated_checksum_dir(argv[1]):
        print(f"{n},{p},{h}")
