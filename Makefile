help:
	@echo "The following make sub-commands are avalable:"
	@echo "docer-run   - run the docker continder"
	@echo "docker-stop - stop the docker"
	@echo "docker      - build a docker containder"
	@echo "dev-run     - run the development server"
	@echo "clean       - remove cache files"
	@echo "help        - show this message"

dev-run:
	uvicorn dirsum.api:app --reload

clean:
	find dirsum tests -name "__pycache__" -type d -exec rm -rf {} \; -prune
	rm -fr .tox build dist dirsum.egg-info

docker: clean
	docker build . --tag dirsum

docker-run: docker
	docker run -p8000:8000 dirsum

docker-stop:
	docker kill $$(docker ps | awk '$$2=="dirsum" {print $$1}')

.PHONEY: help dev-run clean docker docker-run docker-stop
